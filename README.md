# MovieApp

The app searches movie by name using moviedb API: http://api.themoviedb.org/3/search/movie and presents results in a list format. Also, persists top ten search queries.

![](MovieList.png) ![](MovieSearchList.png)


### Introduction

**IDE**: Xcode 9.2
**Language**: Swift 4
**Deployment Target**: iOS 9.0

### Dependency

Pod is used to manage external dependencies
[SDWebImage](https://github.com/rs/SDWebImage) is the only pod added

### App Architecture

MVVM is used to develop the app with use of closures for callbacks from ViewModel to Views.
In addition to MVVM, [App Coordinators](http://khanlou.com/2015/10/coordinators-redux/) are used to control the flow logic of the app.

The reason for choosing MVVM is primarily to keep View layer devoid of any logic and to ensure that unit test cases of business logic do not have dependency on View Controller life cycle.

Coordinators are used to keep ViewControllers/Views dumb and to not enable them to control the app flow, i.e. the view controllers are not going to push/pop new views. This will be performed by Coordinators with delegate methods from ViewModel classes.

The app starts by initialising AppCoordinator in AppDelegate's didFinishLaunchingWithOptions: method. AppCoordinator is the coordinator which handles all other coordinators.

This coordinator creates another coordinator - MovieListCoordinator which initialises MovieListVC with MovieListVM.

MovieListVC is a Viewcontroller/View for list screen and depends on MovieListVM for data and other stuff.

MovieListVM makes use of protocol backed DataProviders like MovieListAPIDataProvider to fetch movie search data from APIs. This is written in such a way that the data provider can be changed without changing ViewModel class, and in turn ensures Open/Closed principle of *SOLID* principles.

To persist search query data, another ViewModel class has been introduced - MovieSearchListVM. This view model class makes use of MovieSearchListDataProvider protocol to fetch persisted results and to persist new result.

MovieSearchListSqliteDataProvider implements MovieSearchListDataProvider and uses CoreData to persist results in .sqlite.

Also, most of the classes follows *Single Responsibility Principle* and View Model classes are made testable via *Dependency Injection*.


