//
//  MovieListVMTests.swift
//  MovieAppTests
//
//  Created by Puneet Sharma2 on 18/12/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import XCTest
import MovieApp

class MockDataProvider:MovieListDataProvider {
    
    private func data(fromJson json:String) -> Data? {
        let bundle = Bundle(for: type(of: self))
        guard let file = bundle.url(forResource: json, withExtension: "json") else {
            return nil
        }
        return try? Data(contentsOf:file)
    }
    
    func fetchMovieListFor(_ query: String, page: Int, completionHandler: @escaping (MovieList?, Error?) -> ()) {
       // Read data from json
        guard let data = self.data(fromJson: query), let list = try? JSONDecoder().decode(MovieJSONList.self, from: data) else {
            completionHandler(nil, nil)
            return
        }
        
        let movieList = MovieListModel(json:list)
        completionHandler(movieList, nil)
        return
    }
    
    func cancelTask() {
     
    }
}

class MovieListVMTests: XCTestCase {
    
    var viewModel:MovieListVM!
    
    override func setUp() {
        super.setUp()
        viewModel =  MovieListVM(dataProvider: MockDataProvider())
    }
    
    override func tearDown() {
        viewModel = nil
        super.tearDown()
    }
    
    func testSearch(){
        var query = "batman"
        viewModel.searchMovieBy(query)
        
        // Test total pages
        XCTAssert(viewModel.numberOfPages() == 5, "Pages should be 5")
        
        // test number of items
        XCTAssert(viewModel.numberOfItemsIn(page: 0) > 0, "Total items count should be greater than zero")
        
        query = "rocky"
        viewModel.searchMovieBy(query)
        XCTAssert(viewModel.numberOfPages() == 4, "Pages should be 4")
        
        query = "Ek Do Teen"
        viewModel.searchMovieBy(query)
        XCTAssert(viewModel.numberOfItemsIn(page: 0) == 0 , "Pages should be 0")
    }
    
    func testListData() {
        let query = "batman"
        viewModel.searchMovieBy(query)
        
        let item = viewModel.movieListItem(at: IndexPath(row: 0, section: 0))
        XCTAssertNotNil(item, "item should not be nil")
        
        XCTAssertEqual(item!.name, "Batman", "name should be batman")
        XCTAssertEqual(item!.releaseDate, "1989-06-23", "release dates should match")
        XCTAssertEqual(item!.votes, "2358 votes", "vote count should match")
        XCTAssertNotNil(item!.overview, "overview message should not be nil")
    }
    
    func testViewModelClosures() {
        var isReloadCalled:Bool = false
        var listState:MovieListState = .startSearch
        
        
        viewModel.reloadList = {
            isReloadCalled = true
        }
        
        viewModel.setViewState = {(state:MovieListState) in
            listState = state
        }
        
        var query = "batman"
        viewModel.searchMovieBy(query)
        
        XCTAssertTrue(isReloadCalled, "reload should have been called")
        XCTAssertEqual(listState.identifier, MovieListState.withData.identifier, "State should be with data")
        
        query = "hello"
        viewModel.searchMovieBy(query)
        
        XCTAssertTrue(isReloadCalled, "reload should have been called")
        XCTAssertEqual(listState.identifier, MovieListState.noData("").identifier, "State should be no data")
    }
        
}
