//
//  MovieListCoordinator.swift
//  MovieApp
//
//  Created by Puneet Sharma2 on 18/12/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import Foundation
import UIKit

/// Coordinator for List Page
class MovieListCoordinator:Coordinator {
    let navigationController:UINavigationController!
    
    init(withNavigationController navigationController:UINavigationController) {
        self.navigationController = navigationController
    }
    
    /// Pushes instance of MovieListVC on navigation stack
    func start() {
        let vm = MovieListVM(dataProvider: MovieListAPIDataProvider())
        let vc = MovieListVC.controller(viewModel: vm)
        self.navigationController .pushViewController(vc, animated: true)
        self.navigationController.setNavigationBarHidden(true, animated: false)
    }
}
