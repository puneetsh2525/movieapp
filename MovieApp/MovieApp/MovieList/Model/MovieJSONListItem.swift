//
//  MovieJSONListItem.swift
//  MovieApp
//
//  Created by Puneet Sharma2 on 17/12/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import Foundation

/// Represents Model classes for JSON data returned from APIs
/// Conforms to Swift 4 Codable protocol for easy coversion of JSON network data to Swift sruct classes and vice-versa
public struct MovieJSONList:Codable {
    let currentPage:Int
    let totalResults:Int
    let totalPages:Int
    let results:[MovieJSONListItem]
    
    private enum CodingKeys:String, CodingKey {
        case currentPage = "page"
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case results = "results"
    }
}

public struct MovieJSONListItem:Codable {
    let voteCount:Int
    let movieId:Int
    let video:Bool
    let voteAverage:Double
    let title:String
    let popularity:Double
    let posterPath:String?
    let originalLanguage:String
    let originalTitle:String
    let backdropPath:String?
    let adult:Bool
    let overview:String
    let releaseDate:String
    let genreIds:[Int]
    
    private enum CodingKeys:String, CodingKey {
        case voteCount = "vote_count"
        case movieId = "id"
        case video = "video"
        case voteAverage = "vote_average"
        case title = "title"
        case popularity = "popularity"
        case posterPath = "poster_path"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case genreIds = "genre_ids"
        case backdropPath = "backdrop_path"
        case adult = "adult"
        case overview = "overview"
        case releaseDate = "release_date"
    }
}
