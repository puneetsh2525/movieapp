//
//  MovieSearchItem+CoreDataProperties.swift
//  
//
//  Created by Puneet Sharma2 on 17/12/17.
//
//

import Foundation
import CoreData

extension MovieSearchItem {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<MovieSearchItem> {
        let request = NSFetchRequest<MovieSearchItem>(entityName: MovieSearchItem.entityName)
        request.sortDescriptors = [NSSortDescriptor(key: "created", ascending: false)]
        return request
    }
    
    @NSManaged public var query: String?
    @NSManaged public var created: NSDate?
}

extension MovieSearchItem {
    convenience init(query:String, context:NSManagedObjectContext) {
        self.init(managedObjectContext: context)
        self.query = query
        self.created = Date() as NSDate
    }
    
    public class func fetchFor(_ query:String, from context:NSManagedObjectContext) -> MovieSearchItem? {
        let fetchRequest:NSFetchRequest<MovieSearchItem> = self.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "query == %@", query)
        return try! context.fetch(fetchRequest).first ?? nil
    }
}
