//
//  MovieListItemTableViewCell.swift
//  MovieApp
//
//  Created by Puneet Sharma2 on 17/12/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import UIKit
import SDWebImage

class MovieListItemTableViewCell: UITableViewCell {

    @IBOutlet weak var posterBack:UIImageView!
    @IBOutlet weak var poster:UIImageView!
    @IBOutlet weak var name:UILabel!
    @IBOutlet weak var releaseNLang:UILabel!
    @IBOutlet weak var overview:UILabel!
    @IBOutlet weak var thumbImage:UIImageView!
    @IBOutlet weak var popularity:UILabel!
    @IBOutlet weak var votes:UILabel!
    
    static var identifier:String = {
        return String(describing:MovieListItemTableViewCell.self)
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        poster.image = nil
        name.text = nil
        releaseNLang.text = nil
        overview.text = nil
        thumbImage.image = nil
        popularity.text = nil
        votes.text = nil
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    public func configureWith(_ movie:MovieListItem) {
        name.text = movie.name
        releaseNLang.text = String(format:"%@ | %@", movie.language, movie.releaseDate)
        overview.text = movie.overview
        votes.text = movie.votes
        popularity.text = String(format:"%0.2f%", movie.popularity)
        thumbImage.image = movie.popularity > 50 ? UIImage(named:"thumb_up") : UIImage(named:"thumb_down")
        let imageUrl = URL(string:MovieSearchResource.moviePosterPath + (movie.posterPath ?? ""))
        poster.sd_setImage(with: imageUrl, placeholderImage: UIImage(named:"default"), options: SDWebImageOptions.highPriority) {[weak self](image, error, cacheType, url) in
            self?.posterBack.image = image
        }
    }
}
