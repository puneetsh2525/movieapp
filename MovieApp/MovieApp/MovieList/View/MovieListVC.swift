//
//  MovieListViewController.swift
//  MovieApp
//
//  Created by Puneet Sharma2 on 17/12/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import UIKit

class MovieListVC: UIViewController, StoryboardIdentifiable {

    @IBOutlet weak var startSearchView:UIView!
    @IBOutlet weak var noResultsView:UIView!
    @IBOutlet weak var errorLabel:UILabel! {
        didSet {
            errorLabel.text = nil
        }
    }
    @IBOutlet weak var loadingView:UIView!
    @IBOutlet weak var searchBar:UISearchBar!
    @IBOutlet weak var tableView:UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.estimatedRowHeight = 180
        }
    }
    @IBOutlet weak var searchList:SearchListView!
    
    private var viewModel:MovieListVM! // ViewModel
    
    class func controller(viewModel:MovieListVM) -> MovieListVC {
        let vc:MovieListVC = UIStoryboard(storyboard: .main).instantiateViewController()
        vc.viewModel = viewModel
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViewModel()
        tableView.tableFooterView = UIView() // Used to remove empty rows from tableview
        configureViewWith(viewModel.state)
    }
    
    /// Initialize ViewModel class and setus up its callback closures
    private func setUpViewModel() {
        
        viewModel.reloadList = {[weak self] in
            self?.tableView.reloadData()
        }
        viewModel.setViewState = {[weak self] (state:MovieListState) in
            self?.configureViewWith(state)
        }
        viewModel.resetSearchBar = {[weak self] (query:String?) in
            self?.searchBar.resignFirstResponder()
            self?.searchBar.text = query
        }
        
        searchList.setUpWithViewModel(viewModel.searchListVM)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// This function configures view appearance based on MovieListState enum. It conveniently makes view heirarchy change by making use of bringSubview(toFront:) method based on different MovieListState
    func configureViewWith(_ state:MovieListState) {
        let prevFrontView = self.view.subviews.last
        let currentFrontView:UIView
        switch state {
        case .noData(let error):
            view.bringSubview(toFront: noResultsView)
            errorLabel.text = error
            currentFrontView = noResultsView
        case .startSearch:
            view.bringSubview(toFront: startSearchView)
            currentFrontView = startSearchView
        case .withData:
            tableView.contentOffset = .zero
            view.bringSubview(toFront: tableView)
            currentFrontView = tableView
        case .loading:
            view.bringSubview(toFront: loadingView)
            currentFrontView = loadingView
        case .searchList:
            view.bringSubview(toFront: searchList)
            currentFrontView = searchList
        }
        if currentFrontView != prevFrontView {
            currentFrontView.alpha = 0
            UIView.animate(withDuration: 0.2, animations: {
                currentFrontView.alpha = 1
            }) { (completion) in
            }
        }
    }
}

// MARK:- TableView Datasource and Delegate
extension MovieListVC:UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfPages()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItemsIn(page: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieListItemTableViewCell.identifier, for: indexPath) as! MovieListItemTableViewCell
        guard let movie = viewModel.movieListItem(at: indexPath) else {
            return cell
        }
        cell.configureWith(movie) // Configures cell with movie item
        return cell
    }
    
    /// Returns a header view for each section
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let textView = UITextView()
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.text = String(format:"Page %d", section+1)
        textView.textColor = UIColor.darkGray
        textView.backgroundColor = UIColor(red: 213/255, green: 213/255, blue: 213/255, alpha: 1.0)
        textView.textContainerInset = UIEdgeInsetsMake(12, 12, 12, 12)
        textView.font = UIFont.boldSystemFont(ofSize: 15.0)
        if viewModel.numberOfItemsIn(page: section) <= 0 {
            // Add indicator view
            let indicatorView = UIActivityIndicatorView(activityIndicatorStyle: .white)
            indicatorView.startAnimating()
            indicatorView.frame = CGRect(x: self.tableView.frame.size.width-30-20, y: 12, width: 20, height: 20)
            textView.addSubview(indicatorView)
        }
        
        return textView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    /// This function is overridden to know when list tablview has scrolled to last row
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if maximumOffset - currentOffset <= 10.0 {
            viewModel.lastRowReached()
        }
    }
}

// MARK:- UISearchBarDelegate and Search related methods
extension MovieListVC:UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else {return}
        viewModel.searchButtonPressedWith(text)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.cancelButtonTapped()
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        viewModel.searchBarTapped()
        return true
    }
}

