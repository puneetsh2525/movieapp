//
//  SearchListView.swift
//  MovieApp
//
//  Created by Puneet Sharma2 on 17/12/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import UIKit

class SearchListView: UIView {
    @IBOutlet weak var tableView:UITableView! {
        didSet {
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.estimatedRowHeight = 44
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    
    private var viewModel:MovieSearchListVM!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setUpWithViewModel(_ vm:MovieSearchListVM) {
        self.viewModel = vm
        self.tableView.tableFooterView = UIView()
        self.viewModel.reloadList = {[weak self] in
            self?.tableView.reloadData()
        }
    }
}

extension SearchListView:UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItemsIn(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchListViewCell", for: indexPath)
        guard let item = viewModel.movieSearchListItem(at: indexPath) else {
            return cell
        }
        cell.textLabel?.text = item.query
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.itemTappedAt(indexPath)
    }
}
