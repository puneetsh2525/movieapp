//
//  MovieSearchListVM.swift
//  MovieApp
//
//  Created by Puneet Sharma2 on 17/12/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import UIKit

class MovieSearchListVM {
    private let dataProvider:MovieSearchListDataProvider
    private var items:[MovieSearchListItem] = []
    
    init(dataProvider:MovieSearchListDataProvider){
        self.dataProvider = dataProvider
    }

    var itemTappedHandler:((MovieSearchListItem)->())?
    var reloadList:(()->())?
}

// MARK:- Public API
extension MovieSearchListVM {
    public func numberOfSections() -> Int {
        return 1
    }
    
    public func numberOfItemsIn(_ section:Int) -> Int {
        return items.count
    }
    
    public func movieSearchListItem(at indexPath: IndexPath) -> MovieSearchListItem? {
        guard indexPath.section < numberOfSections(), indexPath.row < numberOfItemsIn(indexPath.section) else {return nil}
        return items[indexPath.row]
    }
    
    public func fetchSearchListData(handler:@escaping (Bool)->Void) {
        dataProvider.fetchMovieSearchListFor(maxCount: 10) {[weak self](list, error) in
            guard let strongSelf = self else {return}
            guard let list = list, list.count > 0 else {
                handler(false)
                return
            }
            strongSelf.items = list
            strongSelf.reloadList?()
            handler(true)
        }
    }
    
    public func persistQuery(query:String) {
         dataProvider.persistSearchItemFor(query)
    }
}

// MARK: User Action
extension MovieSearchListVM {
    public func itemTappedAt(_ indexPath:IndexPath) {
        guard let item = movieSearchListItem(at: indexPath) else {
            return
        }
        itemTappedHandler?(item)
    }
}
