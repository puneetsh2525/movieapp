//
//  MovieListVM.swift
//  MovieApp
//
//  Created by Puneet Sharma2 on 17/12/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import Foundation

/// An enum to depict state of ViewModel
public enum MovieListState {
    case startSearch
    case noData(String)
    case withData
    case loading
    case searchList
    
    public var identifier:Int {
        switch self {
        case .startSearch:
            return 1
        case .noData:
            return 2
        case .withData:
            return 3
        case .loading:
            return 4
        case .searchList:
            return 5
        }
    }
}

/// A ViewModel class for Movie List ViewController
public class MovieListVM {
    private let dataProvider:MovieListDataProvider // Data provider to fetch search results
    
    private var pages:[[MovieListItem]] = [[]]
    private var currentPage:Int = 1
    private var queryText:String = ""
    private var isSearchInProcess:Bool = false
    
    let searchListVM:MovieSearchListVM
    
    var prevState:MovieListState! = .none
    var state:MovieListState = .startSearch {
        didSet {
            prevState = oldValue.identifier != state.identifier ? oldValue : prevState
            setViewState?(state)
        }
    }
    
    // Callback closures
    public var reloadList:(()->())?
    public var setViewState:((MovieListState)->Void)?
    public var resetSearchBar:((String?)->Void)?
    
    // Init with class that conforms with MovieListDataProvider protocol
    public init(dataProvider:MovieListDataProvider) {
        self.dataProvider = dataProvider
        self.searchListVM = MovieSearchListVM(dataProvider: MovieSearchListSqliteDataProvider())
        self.searchListVM.itemTappedHandler = {[weak self] item in
            self?.searchMovieBy(item.query)
        }
    }
}

// MARK:- Public API
extension MovieListVM {
    
    public func numberOfPages() -> Int {
        return pages.count
    }
    
    public func numberOfItemsIn(page:Int) -> Int {
        return page < pages.count ? pages[page].count : 0
    }
    
    public func movieListItem(at indexPath: IndexPath) -> MovieListItem? {
        guard indexPath.section < numberOfPages(), indexPath.row < numberOfItemsIn(page: indexPath.section) else {return nil}
        return pages[indexPath.section][indexPath.row]
    }

}

// MARK: UserEvents
extension MovieListVM {
    public func searchButtonPressedWith(_ searchText:String) {
        // ask data provider for data with this query
        searchMovieBy(searchText)
    }
    
    public func lastRowReached() {
        // reload next page if possible
        reloadNextPageIfPossible()
    }
    
    public func searchBarTapped() {
        resetSearchBar?(nil)
        // Fetch search data
        searchListVM.fetchSearchListData { [weak self](success) in
            guard let strongSelf = self else {return}
            guard success else { return }
            strongSelf.state = .searchList
        }
    }
    
    public func cancelButtonTapped() {
        state = prevState ?? state
        resetSearchBar?(nil)
    }
}

// MARK: Private API
extension MovieListVM {
    
    private func resetSearchFor(_ query:String) {
        self.dataProvider.cancelTask()
        isSearchInProcess = false // Cancel ongoing task
        pages = [[]]
        self.resetSearchBar?(query)
    }
    
    public func searchMovieBy(_ query:String) {
        state = .loading
        resetSearchFor(query)
        searchMoviesBy(query, page: 1) {[weak self](error) in
            guard let strongSelf = self else {return}
            strongSelf.state = strongSelf.pages.first?.count ?? 0 > 0 ? .withData : .noData(error?.localizedDescription ?? "No movies found. Please try another query.")
            strongSelf.reloadList?()
        }
    }
    
    private func reloadNextPageIfPossible() {
        if currentPage < pages.count {
            searchMoviesBy(queryText, page: currentPage+1){[weak self](error) in
                self?.reloadList?()
            }
        }
    }
    
    private func searchMoviesBy(_ query:String, page:Int = 1, handler:((Error?)->())?) {
        guard isSearchInProcess == false else {
            return
        }
        queryText = query
        currentPage = page
        isSearchInProcess = true
        self.dataProvider.fetchMovieListFor(query, page: page) {[weak self](movieList, error) in
            guard let strongSelf = self else { handler?(error)
                return}
            strongSelf.isSearchInProcess = false
            guard let list = movieList, list.result.count > 0 else { handler?(error)
                return}
            if strongSelf.pages.count == 1 {
                strongSelf.pages = Array<Array<MovieListItem>>(repeating: [], count: list.totalPages)
            }
            strongSelf.pages[page-1] = list.result
            handler?(error)
            strongSelf.searchListVM.persistQuery(query: query)
        }
    }
}
