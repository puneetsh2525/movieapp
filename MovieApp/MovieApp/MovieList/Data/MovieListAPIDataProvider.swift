//
//  MovieListAPIDataProvider.swift
//  MovieApp
//
//  Created by Puneet Sharma2 on 17/12/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import Foundation

// A struct which conforms to APIResource and creates relevant url for Movie Search API
struct MovieSearchResource :APIResource {
    typealias Model = MovieJSONList
    
    static let moviePosterPath:String = "http://image.tmdb.org/t/p/w185/"
    
    let apiKey:String = "2696829a81b1b5827d515ff121700838"
    let query:String
    let page:String
    
    var methodPath: String {
        return "/search/movie"
    }
    
    var queryItems: [URLQueryItem]? {
         return [URLQueryItem(name: "api_key", value: apiKey),URLQueryItem(name: "query", value: query),URLQueryItem(name: "page", value: page)]
    }
}

/// A concrete implementation of MovieListDataProvider protocol where movies are loaded using API requests
class MovieListAPIDataProvider:MovieListDataProvider {
    
    private var apiRequest:APIRequest<MovieSearchResource>?
    
    func fetchMovieListFor(_ query: String, page: Int, completionHandler: @escaping (MovieList?, Error?) -> ()) {
        let resource = MovieSearchResource(query: query, page: String(page))
        let request = APIRequest(resource: resource)
        apiRequest = request
        request.load { (jsonList, error) in
            guard let list = jsonList else {completionHandler(nil, error)
                return}
            let movieList = MovieListModel(json:list)
            completionHandler(movieList, nil)
        }
    }
    
    func cancelTask() {
        apiRequest?.cancelLoad()
    }
}
