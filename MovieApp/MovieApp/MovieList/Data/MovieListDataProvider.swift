//
//  MovieListDataProvider.swift
//  MovieApp
//
//  Created by Puneet Sharma2 on 17/12/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import Foundation

/// Protocol for MovieList
public protocol MovieList {
    var currentPage:Int {get}
    var totalPages:Int {get}
    var result:[MovieListItem] {get}
}

/// Protocol for MovieListItem
public protocol MovieListItem {
    var posterPath:String? {get}
    var name:String {get}
    var releaseDate:String{get}
    var language:String{get}
    var overview:String{get}
    var popularity:Double{get}
    var votes:String{get}
}

/// Protocol for classes that can fetch data by query and page. The classes that conforms to this protocol can fetch data either from API requests, CoreData or plists.
public protocol MovieListDataProvider {
    func fetchMovieListFor(_ query:String, page:Int, completionHandler: @escaping(MovieList?, Error?)->())
    func cancelTask()
}

/// Concrete class which conforms to MovieList. A
/// init method is provided to create from json object. Similar inits can be used to create from XML data or core data entity etc.
public struct MovieListModel:MovieList {
    public let currentPage: Int
    public let totalPages: Int
    public let result: [MovieListItem]
    
    public init(json:MovieJSONList) {
        currentPage = json.currentPage
        totalPages = json.totalPages
        result = json.results.map{MovieListItemModel(json:$0)}
    }
}

/// Concrete class which conforms to MovieListItem
public struct MovieListItemModel:MovieListItem {
    public let posterPath:String?
    public let name:String
    public let releaseDate:String
    public let overview:String
    public let votes: String
    public let language: String
    public let popularity: Double
    
    public init(json:MovieJSONListItem) {
        posterPath = json.posterPath
        name = json.title
        releaseDate = json.releaseDate
        overview = json.overview
        popularity = abs(json.popularity)
        votes = String(json.voteCount) + " votes"
        language = json.originalLanguage
    }
}

