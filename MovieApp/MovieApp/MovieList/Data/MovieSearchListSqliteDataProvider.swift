//
//  MovieSearchListSqliteDataProvider.swift
//  MovieApp
//
//  Created by Puneet Sharma2 on 17/12/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import Foundation
import CoreData

/// This class conforms to MovieSearchListDataProvider. It persists data in sqlite via CoreData framework and fetches result from the same
class MovieSearchListSqliteDataProvider:MovieSearchListDataProvider {
    
    let context:NSManagedObjectContext = CoreDataService.movie.managedObjectContext
    
    func fetchMovieSearchListFor(maxCount:Int, completionHandler: @escaping([MovieSearchListItem]?, Error?)->()) {
        // Fetch from Core Data
        let request:NSFetchRequest<MovieSearchItem> = MovieSearchItem.fetchRequest()
        guard var items = try? context.fetch(request) else {
            completionHandler(nil, StringError("No data found from sqlite"))
            return
        }
        
        // If greater than max count, remove from last
        if maxCount < items.count {
            for index in (maxCount..<items.count).reversed() {
                context.delete(items[index])
                items.remove(at: index)
            }
        }
        let listItems = items.flatMap{MovieSearchListItemModel(entity:$0)}
        completionHandler(listItems, nil)
        
        context.perform {[weak self] in
            try? self?.context.save()
        }
    }
    
    func persistSearchItemFor(_ query:String) {
        if let item = MovieSearchItem.fetchFor(query, from: context) {
            // Already present in core data, update date
            item.created = Date() as NSDate
        } else {
            // Create a new entity
            _ = MovieSearchItem(query: query, context: context)
        }
        context.perform {[weak self] in
            try? self?.context.save()
        }
    }
}
