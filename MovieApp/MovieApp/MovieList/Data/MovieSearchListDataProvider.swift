//
//  MovieSearchListDataProvider.swift
//  MovieApp
//
//  Created by Puneet Sharma2 on 17/12/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import Foundation

// Protocol for creating an item which can represent persisted movie search query
protocol MovieSearchListItem {
    var query:String{get}
}

// Protocol for fetching persisted movie search query either from CoreData or UserDefaults or other such persistent APIs
protocol MovieSearchListDataProvider {
    func fetchMovieSearchListFor(maxCount:Int, completionHandler: @escaping([MovieSearchListItem]?, Error?)->())
    func persistSearchItemFor(_ query:String)
}

// Struct that conforms to MovieSearchListItem and represents a persisted search query
struct MovieSearchListItemModel:MovieSearchListItem {
    let query:String
    
    init?(entity:MovieSearchItem) {
        guard let query = entity.query else {return nil}
        self.query = query
    }
}
