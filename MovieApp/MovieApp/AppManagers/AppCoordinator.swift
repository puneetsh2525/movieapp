//
//  AppCoordinator.swift
//  FlickrImageGallery
//
//  Created by Puneet Sharma2 on 09/11/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator {
    func start()
}

/// AppCoordinator for maintaining other coordinators that are used in the app
class AppCoordinator:Coordinator {
    
    let navigationController:UINavigationController!
    var coordinators:[Coordinator] = []
    
    public init(withNavigationController navigationController:UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let movieListCoordinator = MovieListCoordinator(withNavigationController: self.navigationController)
        movieListCoordinator.start()
        coordinators.append(movieListCoordinator)
    }
}
