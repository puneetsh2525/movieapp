//
//  APIService.swift
//  MovieApp
//
//  Created by Puneet Sharma2 on 17/12/17
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import Foundation

/// API service layer that uses URLSession and Swift 4 Codable protocol to fetch raw data and convert it into concrete model classes
public protocol APIService: class {
    associatedtype Model where Model:Decodable
    func load(withCompletion completion: @escaping (Model?, Error?) -> Void)
    func decode(_ data: Data) -> Model?
    func cancelLoad()
}

extension APIService {
    fileprivate func load(_ url: URL, withCompletion completion: @escaping (Model?, Error?) -> Void) ->  URLSessionTask {
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: url, completionHandler: { [weak self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            guard let data = data else {
                completion(nil, error)
                return
            }
            completion(self?.decode(data), error)
        })
        task.resume()
        return task
    }
}

public protocol APIResource {
    associatedtype Model where Model:Decodable
    var methodPath: String { get }
    var queryItems: [URLQueryItem]? {get}
    func decodeData(data:Data) -> Model?
}

extension APIResource {
    var url: URL{
        let baseUrl = "http://api.themoviedb.org/3"
        var components = URLComponents(string: baseUrl + methodPath)!
        components.queryItems = queryItems
        return components.url!
    }
    
    func decodeData(data:Data) -> Model? {
        let decoder = JSONDecoder()
        do {
            return try decoder.decode(Model.self, from: data)
        }
        catch {
            print(error)
            return nil
        }
    }
}

public class APIRequest<Resource: APIResource> {
    let resource: Resource
    init(resource: Resource) {
        self.resource = resource
    }
    var tasks:[URL:URLSessionTask] = [:]
}

extension APIRequest: APIService {
    public typealias Model = Resource.Model
    public func load(withCompletion completion: @escaping (Resource.Model?, Error?) -> Void) {
        let task = load(resource.url, withCompletion: completion)
        tasks[resource.url] = task
    }
    
    public func decode(_ data: Data) -> Resource.Model? {
        return resource.decodeData(data: data)
    }
    
    public func cancelLoad() {
        tasks[resource.url]?.cancel()
    }
}

