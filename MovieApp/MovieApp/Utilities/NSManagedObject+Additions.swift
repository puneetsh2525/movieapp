//
//  File.swift
//  newspoint
//
//  Created by Puneet Sharma2 on 03/12/17.
//  Copyright © 2017 TImes Internet Limited. All rights reserved.
//

import Foundation
import CoreData

extension NSManagedObject {
    
    // Get entity name from class name
    static var entityName:String {
        let name = NSStringFromClass(self)
        return name.components(separatedBy: ".").last!
    }
    
    // Creates an entity in given managed object context
    convenience init(managedObjectContext: NSManagedObjectContext) {
        let entityName = type(of: self).entityName
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: managedObjectContext)!
        self.init(entity: entity, insertInto: managedObjectContext)
    }
}
