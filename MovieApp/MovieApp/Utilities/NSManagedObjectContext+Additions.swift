//
//  NSManagedObjectContext+Additions.swift
//  newspoint
//
//  Created by Puneet Sharma2 on 03/12/17.
//  Copyright © 2017 TImes Internet Limited. All rights reserved.
//

import Foundation
import CoreData

extension NSManagedObjectContext {
    // Inserts given NSManagedObject into context
    public func insertObject<T: NSManagedObject>() -> T {
        guard let object = NSEntityDescription.insertNewObject(forEntityName: T.entityName, into: self) as? T
            else { fatalError("Invalid Core Data Model.") }
        return object
    }
}


